import react, { useState } from "react";
import TodoItem from "../TodoItem/TodoItem";
import "./Todolist.css"

const Todolist = ({ todo, setTodo, filters }) => {
  // state
  const [inputText, setInputText] = useState('');

  // function add todo
  const inputHandlear = (e) => {
    setInputText(e.target.value)
  }
  const submitTodo = (e) => {
    if (e.key === 'Enter') {
      setTodo([
        ...todo, { text: inputText, completed: false, id: Math.random() * 1000 }
      ]);
      setInputText('');
    }
  }

  return (
    <div className="box-todoList">
      <input value={inputText} onKeyDown={submitTodo} onChange={inputHandlear} type='text' className="todo_input" placeholder="What needs to be done?" />
      <ul className="todo_list">
        {filters.map((item) => {
          return (
            <TodoItem item={item} todo={todo} setTodo={setTodo} />
          )
        })}
      </ul>
    </div>
  )
}
export default Todolist;