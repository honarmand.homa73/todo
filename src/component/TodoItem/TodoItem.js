import React, { useState } from "react";
import "./TodoItem.css"
const TodoItem = ({ item, todo, setTodo }) => {
  // state
  const [edit, setEdit] = useState(item.text);
  const [toggle, setToggle] = useState(true);

  //function deletItem
  const deletTodo = () => {
    setTodo(todo.filter((el) => el.id !== item.id))
  }
  //function check item
  const completedTdo = () => {
    setTodo(todo.map((el) => {
      if (el.id === item.id) {
        return {
          ...el, completed: !el.completed
        }
      }
      return el;
    }
    ));
  }
  // function ubdate
  const updateIput = () => {
    setToggle(false)
  }
  const updateTodo = (e) => {
    if (e.key === 'Enter') {
      setTodo(todo.map((el) => {
        if (el.id === item.id) {
          return { ...el, text: edit }
        }
        return el;
      }
      ))
      setToggle(true)
    }
  }
  function handleChange(event) {
    return (setEdit(event.target.value))
  }

  return (
    <div className="todo_lost-item">
      <div className="todo-text">
        <button type="checkbox" className={`todo-check ${item.completed ? "todo-active" : ""}`} onClick={completedTdo}>
          <span class="material-icons md-48">check</span>
        </button>
        {toggle ?
          <li onDoubleClick={updateIput} className={`todo-item ${item.completed ? "complet" : ""}`}>{item.text}</li> :
          <input type="text" value={edit} onChange={handleChange} onKeyDown={updateTodo} />
        }
      </div>
      <button onClick={deletTodo} className="todo-close">
        <span class="material-icons md-48">close</span>
      </button>
    </div>
  )

}

export default TodoItem;