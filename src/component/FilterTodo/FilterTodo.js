import React from "react";
import "./FilterTodo.css";

const FilterTodo = ({ setStatuse, todo, setTodo }) => {
  // function status
  const statuseHandler = (e) => {
    setStatuse(e.target.innerText)
  }
  // function removeAll
  const removeAll = (el) => {
    setTodo([])
  }

  return (
    <div className="box-filter">
      <div className="filter_item">
        <span>{todo.length}</span>
        <p>item list</p>
      </div>
      <div className="filter_active">
        <button onClick={statuseHandler} className="all_filter">All</button>
        <button onClick={statuseHandler} className="active_filter">Active</button>
      </div>
      <di className="filter_button">
        <button onClick={removeAll}>Creat Completed</button>
      </di>
    </div>
  )
}
export default FilterTodo;