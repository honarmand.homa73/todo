import React, { useState, useEffect } from "react";
import "./App.css";
import 'bootstrap/dist/css/bootstrap.min.css';
// import component
import FilterTodo from "./component/FilterTodo/FilterTodo";
import Todolist from "./component/Todolist/Todolist";



const App = () => {
  const [todo, setTodo] = useState([]);
  const [statuse, setStatuse] = useState('all');
  const [filters, setFilters] = useState([]);

  // useEffect localStoreg
  useEffect(() => {
    getTodoLocal();
  },[])
  // useEffect filter 
  useEffect(() => {
    filterHander();
    saveTodoLocal()
  }, [todo, statuse]);
  // filter
  const filterHander = () => {
    switch (statuse) {
      case "Active":
        setFilters(todo.filter(item => item.completed === false));
        break;
      default:
        setFilters(todo);
        break;
    }
  }
  // savelocal
  const saveTodoLocal = () => {
      localStorage.setItem("todo", JSON.stringify(todo));
  }

  const getTodoLocal = () => {
    if (localStorage.getItem("todo") === null) {
      localStorage.setItem("todo", JSON.stringify([]));
    } else {
     let todoLocal= JSON.parse(localStorage.getItem("todo"))
     setTodo(todoLocal)
    }
  }

  return (
    <>
      <div className="container">
        <div className="row justify-content-center mt-5">
          <div className="col-md-6 col-12">
            <h1 className="titr_todo">
              Todo
            </h1>
            <div className="box-todo ">
              <Todolist todo={todo} setTodo={setTodo} filters={filters} />
            </div>
            <FilterTodo setStatuse={setStatuse} todo={todo} setTodo={setTodo} />

          </div>
        </div>
      </div>
    </>

  )
}

export default App;